# 介绍

AgileBPM 是完全模块化的项目集合，含【流程模块】、【业务表单模块】、【组织架构模块】、【鉴权模块】、【系统模块】、【AO办公模块】、【PC前端项目】、【移动端前端项目】 模块与模块间不耦合，您可以自由选择需要的模块（目前鉴权有两个实现）

* 项目部署、实施文档: http://www.agilebpm.cn/

* 流程实施视频介绍： https://share.weiyun.com/5uuOrvS

* PC在线测试地址: http://test.agilebpm.cn/login.html

* 功能缺陷请在项目上创建建 issues，可以查看已完成issues来寻找问题解决方法

