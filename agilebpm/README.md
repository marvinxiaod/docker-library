## 构建镜像

```
$ ./docker-build.sh
```

## 使用镜像

```
$ docker run -it \
--name agilebpm \
-e active=SIT \
-v TZ=Ansia/Shanghai \
-v /var/log/agilebpm:/usr/local/tomcat/logs \
-v /var/code/agilebpm:/usr/local/tomcat/webapps \
agilebpm:xxx
```

## 环境变量

#### active 

指定运行环境，对应`spring.profiles.active`配置

## 挂载目录

#### `/usr/local/tomcat/logs`

运行时日志产生存放目录

#### `/usr/local/tomcat/webapps`

运行时程序目录




